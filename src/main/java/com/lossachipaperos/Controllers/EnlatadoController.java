package com.lossachipaperos.Controllers;

import com.lossachipaperos.Model.ProductoEnlatado;
import com.lossachipaperos.Repository.ProductoEnlatadoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/lata")
public class EnlatadoController {
    @Autowired
    private ProductoEnlatadoRepositorio repositorio;

    @PostMapping(path = "/agregar")
    public @ResponseBody String agregarNuevaLata(@RequestParam String nombre, @RequestParam Integer frecuencia){
        ProductoEnlatado lata = new ProductoEnlatado();
        lata.setFrecuenciaAbsoluta(frecuencia);
        lata.setNombreProductoEnlatado(nombre);
        repositorio.save(lata);
        return "Añadido";
    }

    @GetMapping(path = "/todos")
    public @ResponseBody Iterable<ProductoEnlatado> obtenerTodos(){
        return repositorio.findAll();
    }
}
