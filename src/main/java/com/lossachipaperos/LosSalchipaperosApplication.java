package com.lossachipaperos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LosSalchipaperosApplication {
    public static void main(String[] args) {
        SpringApplication.run(LosSalchipaperosApplication.class, args);
    }
}
