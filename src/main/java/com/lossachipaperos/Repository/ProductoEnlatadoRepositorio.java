package com.lossachipaperos.Repository;

import com.lossachipaperos.Model.ProductoEnlatado;
import org.springframework.data.repository.CrudRepository;

public interface ProductoEnlatadoRepositorio extends CrudRepository<ProductoEnlatado,Integer> {
}
