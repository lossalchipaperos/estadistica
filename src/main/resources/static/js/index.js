
async function registrarLata() {
    let lata = {};
    lata.nombre = "[ ".concat(document.getElementById('nueva-lata-nombre').value).concat(" )");
    lata.frecuencia = document.getElementById('nueva-lata-frecuencia').value;

    console.log(lata);
    $.post("http://localhost:8080/lata/agregar", lata);
}

document.getElementById('agregar-lata').addEventListener("click", function (event) {
    event.preventDefault();
    registrarLata();
})


async function fetchDataGraph(pathAPI) {
    const request = await fetch(pathAPI);
    const response = await request.json().then(
    )
    let nombres = [];
    let frecuencias = [];
    console.log(response)
    response.forEach(lata =>{
        nombres.push(lata.nombreProductoEnlatado)
        frecuencias.push(lata.frecuenciaAbsoluta)
    });


    await (async function () {
        new Chart(
            document.getElementById('acquisitions'),
            {
                type: 'line',
                data: {
                    labels: nombres,
                    datasets: [
                        {
                            label: 'Dataset',
                            data: frecuencias,
                            borderColor: 'rgb(255, 99, 132)',
                            fill: false,
                            stepped: true,
                        }
                    ]
                },
                options: {
                    responsive: true,
                    interaction: {
                        intersect: false,
                        axis: 'x'
                    },
                    plugins: {
                        title: {
                            display: true,
                            text: (ctx) => 'Frecuencia absoluta por clases',
                        }
                    }
                }
            }
        );
    })();
}


